# Ejada Task

This is project contains producer and consumer based on python


# producer 

1- Read from CSV file (Transaction ID, Customer ID, Time, Product ID, Cost)

2- Send each record to Kafka after some random time delay.
# consumer 

1- Read Records from Kafka using spark streaming

2- Aggregate every window (Window not a single batch) based on Customer ID and export it toCustomer.txt,

3- for the same records aggregate them based on Product ID and save it to Product.txt.

4- Cosumer configration read data from config.json