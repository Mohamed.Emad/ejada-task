import os
import json

os.environ['PYSPARK_SUBMIT_ARGS'] = '--packages org.apache.spark:spark-streaming-kafka-0-8_2.11:2.1.0,org.apache.spark:spark-sql-kafka-0-10_2.11:2.1.0,com.databricks:spark-avro_2.11:3.2.0 pyspark-shell'

from ast import literal_eval
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils




def CreateSparkStreamingContext(checkPointPath):   
    # create spark and streaming contexts
    sc = SparkContext("local[*]", "KafkaDirectStream")
    ssc = StreamingContext(sc, 5)
    # defining the checkpoint directory
    ssc.checkpoint(checkPointPath)
    return ssc




def LoadConfigrations():
    with open('config.json', 'r') as f:
        config = json.load(f)
    return config




def ConsumeKafka(topic,bootStrapServers,KafkaConsumerGroup,ssc):
    #consume kafka topic
    kafkaStream = KafkaUtils.createDirectStream(ssc, [topic], {'bootstrap.servers': bootStrapServers,
            'auto.offset.reset': 'smallest', 'group.id': KafkaConsumerGroup })  
    return kafkaStream




def AggregateCSVWindow(kafkaStream,aggregateColumnIndex,aggregateColumnName): 
    
    columnDstream = kafkaStream.map(lambda x: str(x).split(',')[aggregateColumnIndex] ) 
    # Count each value and number of occurences in the batch 
    #windowed Window size 60
    countValuesWindowed = columnDstream.countByValueAndWindow(60,5).transform(lambda rdd:rdd                                  .sortBy(lambda x:-x[1]))                                  .map(lambda x:"%s,%s" % (x[0],x[1]))
    #Write to file
    countValuesWindowed.foreachRDD(lambda rdd: rdd.foreach(lambda rec: open(aggregateColumnName+".txt", "a").write(rec+"\n")))
    
 



config = LoadConfigrations()
ssc = CreateSparkStreamingContext(config["checkPointPath"])
kafkaStream = ConsumeKafka(config["KafkaTopic"],config["bootstrapServer"],config["KafkaConsumerGroup"],ssc)
#Aggregate based on Customer ID
AggregateCSVWindow(kafkaStream,2,'Customer')
AggregateCSVWindow(kafkaStream,4,'Product')
ssc.start()
ssc.awaitTermination()

